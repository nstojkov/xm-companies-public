package main

import (
	"encoding/json"
	"fmt"
	"io"
	"log"
	"net/http"
	"os"

	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/middleware/cors"
	"github.com/meilisearch/meilisearch-go"
)

type Response []struct {
	ID          string `json:"Symbol"`
	CompanyName string `json:"Company Name"`
}

func main() {
	client := meilisearch.NewClient(meilisearch.ClientConfig{
		Host: "http://127.0.0.1:7700",
	})
	index := client.Index("companies")

	resp, err := http.Get("https://pkgstore.datahub.io/core/nasdaq-listings/nasdaq-listed_json/data/a5bc7580d6176d60ac0b2142ca8d7df6/nasdaq-listed_json.json")
	if err != nil {
		log.Fatal("No response")
	}
	defer resp.Body.Close()
	body, err := io.ReadAll(resp.Body)

	var result Response
	if err := json.Unmarshal(body, &result); err != nil {
		log.Fatal("JSON Error: Cannot unmarshal")
	}
	res, err := json.Marshal(&result)

	task, err := index.AddDocuments(res)
	_ = task
	if err != nil {
		log.Fatal(err)
		os.Exit(1)
	}

	app := fiber.New()

	// Obviously not prod ready code
	app.Use(cors.New(cors.Config{
		AllowOrigins: "*",
		AllowHeaders: "Origin, Content-Type, Accept",
	}))

	// Just a healthcheck, delete this
	app.Get("/", func(c *fiber.Ctx) error {
		return c.SendString("Alive")
	})

	app.Get("/search", func(c *fiber.Ctx) error {
		searchQuery := c.Query("s")

		searchRes, err := client.Index("companies").Search(searchQuery,
			&meilisearch.SearchRequest{})
		if err != nil {
			fmt.Println(err)
			os.Exit(1)
		}

		str, err := json.Marshal(searchRes.Hits)
		return c.SendString(string(str))
	})

	app.Listen(":3001")
}
