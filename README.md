# Fetch Listed Companies
A small microservice that queries our [Meilisearch](https://www.meilisearch.com/) for companies and their symbols.

## Why:
In the future we can have lots more exchanges other than the NASDAQ one we have now, so through this service we can filter and return through all of them, and can handle stuff like delistings and new additions to the exchange.

We can also cache this pretty aggressively.

Meilisearch is used for convenience and speed instead of a DB. Currently returns search queries with the default limit in ~4ms. 

Go tests not included
